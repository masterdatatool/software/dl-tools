#!/bin/bash

if [ "$EUID" -ne 0 ]; then
  echo "This script must be run as root / sudo"
  exit 1
fi

if [ -d "/opt/dl-tools" ]; then

    echo "Deleting /opt/dl-tools folder"
    rm -rf /opt/dl-tools
    
fi

if ! [ -d "/opt/dl-tools" ]; then

    echo "Generating /opt/dl-tools folder"
    mkdir /opt/dl-tools
    mkdir /opt/dl-tools/dl-lirc-setup
    mkdir /opt/dl-tools/dl-receive
    
fi

echo "Copying dl-lirc-setup.py"
cp dl-lirc-setup/lirc-setup.py /opt/dl-tools/dl-lirc-setup/lirc-setup.py
echo "Copying dl-receive.py"
cp dl-receive/dl-receive.py /opt/dl-tools/dl-receive/dl-receive.py

echo "Copying lirc-setup.service"
cp dl-lirc-setup/lirc-setup.service /lib/systemd/system/lirc-setup.service

echo "Enabling lirc-setup.service"
systemctl enable lirc-setup

echo "Starting lirc-setup"
systemctl restart lirc-setup

