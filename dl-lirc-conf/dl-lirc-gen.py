import sys

def hex_to_binary(hex_data):
    # Convert hex to binary
    binary_data = bin(int(hex_data, 16))[2:]
    
    # Calculate the number of zeros needed for padding
    padding_length = (8 - len(binary_data) % 8) % 8
    
    # Pad with zeros
    binary_data = '0' * padding_length + binary_data
    
    return binary_data

def binary_to_lirc_conf(binary_data):
    # Split the binary data into individual bits
    bits = [bit for bit in binary_data]
    bits.insert(0, "0")
    bits.append("S")
    print(bits)

    last_bit1 = False
    last_bit0 = False
    count = 0
    new_line_content = "\n             "
    pulse = "1559     "
    space_b0 = "1559     "
    space_b1 = "7825     "
    space_equal = "4690     "
    space_stop = "10954     "


    # Message start
    lirc_conf = f"             1562     1559     1568     1559     1569    14077\n             "
    
    # Generate raw codes based on the binary data
    for bit in bits:

        if bit == '0':

            lirc_conf += pulse

            if last_bit0 == True:
                lirc_conf += space_equal
            else:
                lirc_conf += space_b0
            
            count = count + 1
            last_bit0 = True
            last_bit1 = False

        elif bit == '1':

            lirc_conf += pulse

            if last_bit1 == True:
                lirc_conf += space_equal
            else:
                lirc_conf += space_b1
            
            count = count + 1
            last_bit0 = False
            last_bit1 = True

        elif bit == 'S':

            lirc_conf += pulse
            lirc_conf += space_stop

        if count == 3:
            lirc_conf += new_line_content
            count = 0
    

    return lirc_conf

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python lirc-gen.py <hex_data>")
        sys.exit(1)

    hex_data = sys.argv[1]

    # Convert hex to binary
    binary_data = hex_to_binary(hex_data)
    print(binary_data)

    # Generate LIRC configuration
    lirc_conf = binary_to_lirc_conf(binary_data)

    # Print the generated LIRC configuration
    print(lirc_conf)
