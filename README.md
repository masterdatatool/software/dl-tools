# DL-TOOLS
Collection of Python scripts for handling the DataLink (DL) communication protocol through the MasterDataTool.

## INSTALLATION
Simply clone this repository and execute the install.sh shell script as root (sudo).<br>
It will create a folder in your /opt directory and install all files there.<br>

`git clone https://gitlab.com/masterdatatool/software/dl-tools`<br>
`cd dl-tools`<br>
`chmod +x install.sh`<br>
`sudo install.sh`<br>

## dl-lirc-gen.py
Use this python script for generating an output of raw commands that can be directly copied into a lirc.conf file.

`python3 dl-lirc-gen.py 0160`<br>

This would enerate a raw output that sends a volume up (0x60) command to the radio (0x01) source.<br>
Copy the output to an existing lirc remote config file. In the same directory you'll also find an example conf file (aal.conf).

## dl-receive.py
A simple tool you can use for debugging any incomming DataLink messages. It uses the mode2 raw lirc tool and parses it's ouput. Resulting from this it wil print out the received comand in hex format.

## lirc-setup
Currently not needed. Lirc works fine just after installing an enabling it.