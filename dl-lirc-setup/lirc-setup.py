import os
from re import U
import subprocess
import time

lirc0 = ""

os.system("systemctl stop lircd.socket")
os.system("systemctl stop lircd")
os.system("killall lircd")
time.sleep(1)
os.system("mkdir -p /run/lirc")


cmd = "udevadm info -a /dev/lirc0 | grep gpio-ir-tx"
subprocess1 = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
lirc0 = subprocess1.stdout.read().decode("utf-8").rstrip('\n').replace(" ", "")

if "gpio-ir-tx" in lirc0:
    print("lirc0 is tx")
    os.system("/usr/sbin/lircd --pidfile=/run/lirc/lircd.pid --device=/dev/lirc0")
else:
    print("lirc1 is tx")
    os.system("/usr/sbin/lircd --pidfile=/run/lirc/lircd.pid --device=/dev/lirc1")