import subprocess
import os

def run_mode2(rx_on_lirc1):
    with open(os.devnull, 'w') as null_file:
        if rx_on_lirc1:
            process = subprocess.Popen(['sudo', 'mode2', '--driver', 'default', '--device', '/dev/lirc1'], stdout=subprocess.PIPE, stderr=null_file, text=True)
        else:
            process = subprocess.Popen(['sudo', 'mode2', '--driver', 'default', '--device', '/dev/lirc0'], stdout=subprocess.PIPE, stderr=null_file, text=True)
        return process.stdout


def parse_mode2_output(output):

    space_values = []
    start = False
    last1 = False
    last0 = False

    for line in output:
        if "space" in line:
            spaceint = int(line.split(" ")[1].replace("\n", ""))
            if 1000 <= spaceint <= 2000:
                # logic ZERO
                if start:
                    last0 = True
                    last1 = False
                    space_values.append(0)
            if 13000 <= spaceint <= 15000:
                # logic START
                start = True
            if 10000 <= spaceint <= 12000:
                # logic END
                break
            if 4000 <= spaceint <= 5000:
                # logic SAME
                if start:
                    if last1:
                        space_values.append(1)
                    else:
                        space_values.append(0)
            if 7000 <= spaceint <= 8500:
                # logic ONE
                if start:
                    last1 = True
                    last0 = False
                    space_values.append(1)

        if "timeout" in line:
            break

    if (len(space_values)) > 1:
        hex_list = [f'{i:02X}' for i in (int(''.join(map(str, space_values[i:i+8])), 2) for i in range(0, len(space_values), 8))]
        print(hex_list)


if __name__ == "__main__":

    cmd = "udevadm info -a /dev/lirc0 | grep gpio-ir-tx"
    subprocess1 = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    lirc0 = subprocess1.stdout.read().decode("utf-8").rstrip('\n').replace(" ", "")

    rx_on_lirc1 = False

    if "gpio-ir-tx" in lirc0:
        print("lirc1 is rx")
        rx_on_lirc1 = True
    else:
        print("lirc0 is rx")
        rx_on_lirc1 = False

    # Datalink receive buffer active low on GPIO12
    os.system("raspi-gpio set 12 op")
    # Enable it
    os.system("raspi-gpio set 12 dl")

    while True:
        parse_mode2_output(run_mode2(rx_on_lirc1))
